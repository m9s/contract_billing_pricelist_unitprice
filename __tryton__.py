# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Billing Pricelist Unitprice',
    'name_de_DE': 'Vertragsverwaltung Abrechnung Preislisten Einzelpreis',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds handling of unit price field, when using modules
    account_invoice_billing_pricelist_unitprice and
    contract_billing_pricelist
    ''',
    'description_de_DE': '''
    - Fügt Prozeduren für das Feld Einzelpreis hinzu bei Verwendung der Module
    account_invoice_billing_pricelist_unitprice und
    contract_billing_pricelist
    ''',
    'depends': [
        'account_invoice_billing_pricelist_unitprice',
        'contract_billing_pricelist',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
