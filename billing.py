# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    def __init__(self):
        super(BillingLine, self).__init__()
        self.product = copy.copy(self.product)
        if self.product.on_change is None:
            self.product.on_change = []
        for field in ['product', 'party', 'billing_date', 'company',
                'quantity', 'contract']:
            if field not in self.product.on_change:
                self.product.on_change += [field]
        self.quantity = copy.copy(self.quantity)
        if self.quantity.on_change is None:
            self.quantity.on_change = []
        for field in ['product', 'party', 'billing_date', 'company',
                'quantity', 'contract']:
            if field not in self.quantity.on_change:
                self.quantity.on_change += [field]
        self._reset_columns()

    def _get_pricelist_from_vals(self, vals):
        contract_obj = Pool().get('contract.contract')
        res = super(BillingLine, self)._get_pricelist_from_vals(vals)
        if vals.get('contract'):
            contract = contract_obj.browse(vals['contract'])
            res = contract.pricelist
        return res

BillingLine()
